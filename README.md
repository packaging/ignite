# [Ignite](https://github.com/weaveworks/ignite) apt packages

### Add repo signing key to apt

```bash
sudo curl -sL -o /etc/apt/trusted.gpg.d/morph027-ignite.asc https://packaging.gitlab.io/ignite/gpg.key
```

### Add repo to apt

```bash
echo "deb https://packaging.gitlab.io/ignite ignite main" | sudo tee /etc/apt/sources.list.d/morph027-ignite.list
```
